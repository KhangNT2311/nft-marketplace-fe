import { Box, ButtonBase, Grid, Tab, Tabs, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import inventBg from "../../assets/images/invent-bg.png";
import buttonBack from "../../assets/images/button-back.png";
import {
  DrakerItem,
  DrakerItemProps,
} from "components/molecules/DrakerItem/DrakerItem";
import { InventoryDrakerItem } from "components/molecules/InventoryDrakerItem/InventoryDrakerItem";
import useTokenIdDraker from "../../zustand/useTokenIdDraker";
import { InventoryDraker } from "components/organisms/InventoryDraker/InventoryDraker";
import { InventoryOnSale } from "components/organisms/InventoryOnSale/InventoryOnSale";
import { Header } from "components/organisms/Header/Header";
import { BsArrowLeft } from "react-icons/bs";

const axios = require("axios");

interface InventoryProps {}

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <Box
      sx={{ height: "100%" }}
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box
          sx={{
            p: 3,
            height: "100%",
          }}
        >
          {children}
        </Box>
      )}
    </Box>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export const Inventory: React.FC<InventoryProps> = ({}) => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <>
      <Grid
        sx={{
          backgroundColor: "#13161b",
          width: "100vw",
          height: "100vh",
          overflow: "hidden",
        }}
      >
        <Header title={"Inventory"} />
        <Link to={"/"} style={{ width: "100px", fontSize: "40px", color: "" }}>
          <BsArrowLeft />
        </Link>
        <Grid
          sx={{
            width: "100%",
            height: "100%",
            overflow: "hidden",
            display: "flex",
            justifyContent: "center",
            position: "relative",
          }}
        >
          <Grid
            sx={{
              width: "90%",
              height: "70vh",
              overflow: "hidden",
              padding: "3%",
              background: "#00000029",
              display: "flex",
              borderRadius: "20px",
            }}
          >
            <Box sx={{ width: "100%", height: "100%" }}>
              <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                <Tabs
                  value={value}
                  onChange={handleChange}
                  aria-label="basic tabs example"
                >
                  <Tab
                    sx={{ color: "#fff" }}
                    label="On Inventory"
                    {...a11yProps(0)}
                  />
                  <Tab
                    sx={{ color: "#fff" }}
                    label="On Sale"
                    {...a11yProps(1)}
                  />
                </Tabs>
              </Box>
              <TabPanel value={value} index={0}>
                <InventoryDraker />
              </TabPanel>
              <TabPanel value={value} index={1}>
                <InventoryOnSale />
              </TabPanel>
            </Box>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};
