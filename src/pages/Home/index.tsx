import { Grid, Typography } from "@mui/material";
import ConnectWalletModal from "components/organisms/ConnectWalletModal/ConnectWalletModal";
import GlobalModal from "components/organisms/GlobalModal/GlobalModal";
import { Header } from "components/organisms/Header/Header";
import { Market } from "components/organisms/Market/Market";
import { ModalLoginError } from "components/organisms/ModalLoginError/ModalLoginError";
import { ModalOrderDraker } from "components/organisms/ModalOrderDraker/ModalOrderDraker";
import { Navbar } from "components/organisms/Navbar/Navbar";
import { useActiveWeb3React } from "hooks";
import useAccount from "hooks/useAccount";
import React, { useEffect } from "react";

export const Home: React.FC = ({}) => {
  const { account } = useActiveWeb3React();

  return (
    <Grid sx={{ height: "100vh", overflow: "hidden" }}>
      <Grid
        container
        sx={{ backgroundColor: "#13161b", width: "100%", overflowX: "hidden" }}
      >
        <Grid
          item
          xs={12}
          md={4}
          lg={3}
          xl={2}
          sx={{ height: "100%", zIndex: "10" }}
        >
          <Navbar />
        </Grid>
        <Grid
          item
          xs={12}
          md={8}
          lg={9}
          xl={10}
          sx={{
            height: "auto",
            zIndex: "5",
          }}
        >
          <Header title={"Marketplace"} />
          <Market />
        </Grid>
        {/* <GlobalModal /> */}
        <ModalLoginError />
      </Grid>
      <ModalOrderDraker />
    </Grid>
  );
};
