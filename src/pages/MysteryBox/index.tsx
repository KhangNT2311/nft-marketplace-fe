import { Box, ButtonBase, Grid } from "@mui/material";
import { BoxList } from "components/organisms/BoxList/BoxList";
import React from "react";
import { Link } from "react-router-dom";
import bgImg from "../../assets/images/mystery-box-bg.jpg";
import buttonBack from "../../assets/images/button-back.png";

interface MysteryBoxProps {}

export const MysteryBox: React.FC<MysteryBoxProps> = ({}) => {
  return (
    <Grid
      sx={{
        width: "100vw",
        height: "100vh",
        overflow: "hidden",
        // backgroundColor: "#13161b",
        backgroundImage: `url(${bgImg})`,
        backgroundSize: "cover",
        backgroundPosition: "center",
      }}
    >
      <Link
        to={"/"}
        style={{ position: "absolute", left: "10px", top: "10px" }}
      >
        <Box
          component={"img"}
          src={buttonBack}
          alt="button back"
          sx={{ width: "80px" }}
        />
      </Link>
      <Grid sx={{ width: "100%", height: "100%" }}>
        <BoxList />
      </Grid>
      <Grid sx={{width: '300px', height: '300px'}}>

      </Grid>
    </Grid>
  );
};
