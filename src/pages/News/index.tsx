import { Box, CircularProgress, Grid, Typography } from "@mui/material";
import { BoxItem } from "components/molecules/BoxItem/BoxItem";
import { NewsBanner } from "components/organisms/NewsBanner/NewsBanner";
import { NewsList } from "components/organisms/NewsList/NewsList";
import useFecthData from "hooks/useFetchData";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { BsArrowLeft } from "react-icons/bs";

const axios = require("axios");

interface indexProps {}

export const News: React.FC<indexProps> = ({}) => {
  const DOMAIN = process.env.REACT_APP_API_DOMAIN_FRONTEND;
  const apiLink = `${DOMAIN}/v1/news`;
  const [pageNumber, setPageNumber] = useState(1);
  const { dataList, loading, hasMoreNews } = useFecthData(apiLink);

  const bannerData = dataList.slice(0, 6);

  return (
    <Grid
      sx={{
        backgroundColor: "#13161b",
        width: "100vw",
        minHeight: "100vh",
      }}
    >
      <Box sx={{ width: "100%", position: "relative" }}>
        <Link
          to={"/"}
          style={{
            position: "absolute",
            left: "10px",
            top: "20px",
            color: "#fff",
            fontSize: "30px",
            fontWeight: "bold",
          }}
        >
          <BsArrowLeft />
        </Link>
        <Typography
          component={"h2"}
          sx={{
            //   position: "fixed",
            top: "0",
            left: "0",
            width: "100%",
            textAlign: "center",
            color: "#fff",
            fontSize: "45px",
            padding: "20px 0",
            zIndex: "10",
            backgroundColor: "#00000029",
          }}
        >
          News
        </Typography>
      </Box>
      <Box sx={{ width: "100%" }}>
        <NewsBanner bannerData={bannerData} />
        <Grid
          sx={{
            maxWidth: { xs: "375px", sm: "650px", md: "1350px", lg: "1850px" },
            margin: "auto",
          }}
        >
          <NewsList newsListData={dataList} />
        </Grid>
        {loading && (
          <Box
            sx={{
              width: "100%",
              height: "auto",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <CircularProgress />
          </Box>
        )}
      </Box>
    </Grid>
  );
};
