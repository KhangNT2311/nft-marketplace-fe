import { Box, Grid, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import nftImage from "../../assets/images/fullbody/axie-full-transparent2.png";
import skill1 from "../../assets/images/skill/beast-back-02.png";
import skill2 from "../../assets/images/skill/aquatic-tail-02.png";
import skill3 from "../../assets/images/skill/reptile-mouth-08.png";
import iconAtk from "../../assets/images/icon/icon-atk.png";
import iconDef from "../../assets/images/icon/icon-def.png";
import iconSpeed from "../../assets/images/icon/flash.png";
import iconFire from "../../assets/images/fire.svg";
import useTokenIdDraker from "../../zustand/useTokenIdDraker";
import { useParams } from "react-router-dom";
import useGetDrakerDetail from "hooks/useGetDrakerDetail";
import drakerInfo from "config/drakerInfo";
const axios = require("axios");

interface indexProps {}

export const Detail: React.FC<indexProps> = ({}) => {
  const DOMAIN_API = process.env.REACT_APP_API_DOMAIN_FRONTEND;
  const { tokenId } = useParams();
  const { draker } = useGetDrakerDetail(`${DOMAIN_API}/v1/drakers/`, tokenId);
  console.log("🚀 ~ file: index.tsx ~ line 43 ~ draker", draker);

  return (
    <>
      {draker && (
        <Grid
          sx={{
            width: "100vw",
            height: "100vh",
            overflow: "hidden",
            backgroundColor: "#13161b",
          }}
        >
          <Grid container sx={{ height: "100%" }}>
            <Grid
              item
              xs={12}
              md={7}
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Box
                component={"img"}
                src={draker.results[0].drakerImage}
                alt="draker image"
                sx={{ width: "70%" }}
              />
            </Grid>
            <Grid
              item
              xs={12}
              md={5}
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Grid
                sx={{
                  width: "80%",
                  height: "80%",
                  overflow: "scroll",
                  backgroundColor: "#1c1f25",
                  border: "1px solid #282c34",
                  borderRadius: "12px",
                }}
              >
                <Grid
                  sx={{
                    width: "100%",
                    padding: "10px",
                    borderBottom: "1px solid #282c34",
                  }}
                >
                  <Typography
                    component={"p"}
                    sx={{ color: "#fff", fontWeight: "bold", fontSize: "30px" }}
                  >
                    Detail
                  </Typography>
                </Grid>
                <Box sx={{ padding: "10px 20px" }}>
                  <Box
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                      paddingBottom: "20px",
                      borderBottom: "1px solid #282c34",
                    }}
                  >
                    <Grid
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        flexDirection: "column",
                      }}
                    >
                      <Typography
                        component={"p"}
                        sx={{
                          color: "#b4bccb",
                          textTransform: "uppercase",
                          fontWeight: "bold",
                          marginLeft: "10px",
                        }}
                      >
                        Class
                      </Typography>
                      <Box
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          flexDirection: "row",
                        }}
                      >
                        <Box
                          component={"img"}
                          src={
                            drakerInfo.typeIcon[draker.results[0].drakerClass]
                          }
                          alt="icon atk"
                          sx={{ width: "20px", marginRight: "10px" }}
                        />
                        <Typography
                          component={"p"}
                          sx={{ color: "#fff", fontWeight: "bold" }}
                        >
                          {draker.results[0].drakerClass}
                        </Typography>
                      </Box>
                    </Grid>
                    <Box>
                      <Typography
                        component={"p"}
                        sx={{ color: "#fff", fontWeight: "bold" }}
                      >
                        Price: ${draker.results[0].price}
                      </Typography>
                    </Box>
                  </Box>
                </Box>
                <Box sx={{ padding: "25px 15px" }}>
                  <Grid
                    sx={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "space-between",
                      flexDirection: "row",
                      paddingBottom: "20px",
                      borderBottom: "1px solid #282c34",
                    }}
                  >
                    <Grid
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        flexDirection: "column",
                      }}
                    >
                      <Typography
                        component={"p"}
                        sx={{
                          color: "#b4bccb",
                          textTransform: "uppercase",
                          fontWeight: "bold",
                          marginLeft: "10px",
                        }}
                      >
                        Attack
                      </Typography>
                      <Box
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          flexDirection: "row",
                        }}
                      >
                        <Box
                          component={"img"}
                          src={iconAtk}
                          alt="icon atk"
                          sx={{ width: "20px", marginRight: "10px" }}
                        />
                        <Typography
                          component={"p"}
                          sx={{ color: "#fff", fontWeight: "bold" }}
                        >
                          {draker.results[0].attributeElements.attack}
                        </Typography>
                      </Box>
                    </Grid>
                    <Grid
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        flexDirection: "column",
                      }}
                    >
                      <Typography
                        component={"p"}
                        sx={{
                          color: "#b4bccb",
                          textTransform: "uppercase",
                          fontWeight: "bold",
                          marginLeft: "10px",
                        }}
                      >
                        Defend
                      </Typography>
                      <Box
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          flexDirection: "row",
                        }}
                      >
                        <Box
                          component={"img"}
                          src={iconDef}
                          alt="icon atk"
                          sx={{ width: "20px", marginRight: "10px" }}
                        />
                        <Typography
                          component={"p"}
                          sx={{ color: "#fff", fontWeight: "bold" }}
                        >
                          {draker.results[0].attributeElements.defend}
                        </Typography>
                      </Box>
                    </Grid>
                    <Grid
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        flexDirection: "column",
                      }}
                    >
                      <Typography
                        component={"p"}
                        sx={{
                          color: "#b4bccb",
                          textTransform: "uppercase",
                          fontWeight: "bold",
                          marginLeft: "10px",
                        }}
                      >
                        Speed
                      </Typography>
                      <Box
                        sx={{
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          flexDirection: "row",
                        }}
                      >
                        <Box
                          component={"img"}
                          src={iconSpeed}
                          alt="icon atk"
                          sx={{ width: "20px", marginRight: "10px" }}
                        />
                        <Typography
                          component={"p"}
                          sx={{ color: "#fff", fontWeight: "bold" }}
                        >
                          {draker.results[0].attributeElements.speed}
                        </Typography>
                      </Box>
                    </Grid>
                  </Grid>
                </Box>
                <Box sx={{ padding: "20px 25px" }}>
                  <Typography
                    component={"p"}
                    sx={{
                      color: "#fff",
                      fontWeight: "bold",
                      marginBottom: "10px",
                    }}
                  >
                    Parts
                  </Typography>
                  <Grid container spacing={4}>
                    <Grid
                      item
                      xs={12}
                      md={6}
                      sx={{
                        display: "flex",
                        flexDirection: "column",
                      }}
                    >
                      <Box
                        sx={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          justifyContent: "center",
                          marginBottom: "5px",
                        }}
                      >
                        <Box
                          component={"img"}
                          src={drakerInfo.parts.beast}
                          alt="beast"
                          sx={{ width: "32px" }}
                        />
                        <Typography
                          component={"p"}
                          sx={{
                            fontWeight: "bold",
                            marginLeft: "5px",
                            textTransform: "uppercase",
                            color: "#b4bccb",
                            fontSize: "10px",
                          }}
                        >
                          Beast
                        </Typography>
                      </Box>
                      <Box
                        component={"img"}
                        src={drakerInfo.beast[draker.results[0].parts.beast]}
                        sx={{ height: "70%" }}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      md={6}
                      sx={{
                        display: "flex",
                        flexDirection: "column",
                      }}
                    >
                      <Box
                        sx={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          justifyContent: "center",
                          marginBottom: "5px",
                        }}
                      >
                        <Box
                          component={"img"}
                          src={drakerInfo.parts.tail}
                          alt="Tail"
                          sx={{ width: "32px" }}
                        />
                        <Typography
                          component={"p"}
                          sx={{
                            fontWeight: "bold",
                            marginLeft: "5px",
                            textTransform: "uppercase",
                            color: "#b4bccb",
                            fontSize: "10px",
                          }}
                        >
                          Tail
                        </Typography>
                      </Box>
                      <Box
                        component={"img"}
                        src={drakerInfo.tail[draker.results[0].parts.tail]}
                        sx={{ width: "80%" }}
                      />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      md={6}
                      sx={{ display: "flex", flexDirection: "column" }}
                    >
                      <Box
                        sx={{
                          display: "flex",
                          flexDirection: "row",
                          alignItems: "center",
                          justifyContent: "center",
                          marginBottom: "5px",
                        }}
                      >
                        <Box
                          component={"img"}
                          src={drakerInfo.parts.body}
                          alt="body"
                          sx={{ width: "32px" }}
                        />
                        <Typography
                          component={"p"}
                          sx={{
                            fontWeight: "bold",
                            marginLeft: "5px",
                            textTransform: "uppercase",
                            color: "#b4bccb",
                            fontSize: "10px",
                          }}
                        >
                          body
                        </Typography>
                      </Box>
                      <Box
                        component={"img"}
                        src={drakerInfo.body[draker.results[0].parts.body]}
                      />
                    </Grid>
                  </Grid>
                </Box>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      )}
    </>
  );
};
