import { Box, Grid, Link, Typography } from "@mui/material";
import React from "react";
import "./index.scss";
import { HiOutlineArrowNarrowRight } from "react-icons/hi";
import newLoadFail from "../../../assets/images/news/new-load-failed.jpg";

interface NewsItemProps {
  title: string;
  img?: string;
  author?: string;
  descriptiion: string;
  url?: string;
  createdAt?: string;
  index: number;
}

export const NewsItem: React.FC<NewsItemProps> = ({
  title,
  img,
  author,
  descriptiion,
  url,
  createdAt,
  index,
}) => {
  return (
    <Grid
      container
      className={index % 2 == 0 ? "news__item__reverse" : ""}
      sx={{
        display: "flex",
        flexDirection: "row",
        width: "100%",
        height: { xs: "300px", md: "400px", lg: "450px" },
        borderRadius: "20px",
        overflow: "hidden",
      }}
    >
      <Grid item xs={12} md={6} sx={{ height: "100%" }}>
        {img ? (
          <Box
            component={"img"}
            src={img}
            onError={({ currentTarget }) => {
              currentTarget.src = newLoadFail;
            }}
            alt="news image"
            sx={{ width: "100%", height: "100%", objectFit: "cover" }}
          />
        ) : (
          <Box
            component={"img"}
            src={newLoadFail}
            alt="news image"
            sx={{ width: "100%", height: "100%", objectFit: "cover" }}
          />
        )}
      </Grid>
      <Grid
        item
        xs={12}
        md={6}
        sx={{
          padding: "0 20px",
          color: "#fff",
          position: "relative",
          height: "100%",
        }}
      >
        <Typography
          component={"p"}
          className="news__title"
          sx={{
            width: "100%",
            height: "40px",
            fontSize: "30px",
            fontWeight: "bold",
            lineHeight: "35px",
            overflow: "hidden",
            textOverflow: "ellipsis",
            textDecoration: "none",
            whiteSpace: "wrap",
            color: "#7498ff",
          }}
        >
          {title}
        </Typography>
        <Typography component={"p"} sx={{ margin: "15px 0" }}>
          <Typography
            component={"span"}
            sx={{ marginRight: "5px", fontWeight: "bold" }}
          >
            {author}
          </Typography>
          on
          {createdAt && (
            <Typography component={"span"} sx={{ marginLeft: "5px" }}>
              {createdAt.slice(0, 10)}
            </Typography>
          )}
        </Typography>
        <div
          className="news__content"
          dangerouslySetInnerHTML={{ __html: descriptiion }}
        ></div>
        <Link
          href={url}
          sx={{
            backgroundColor: "#fff",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            width: "150px",
            height: "40px",
            textDecoration: "none",
            borderRadius: "10px",
            position: "absolute",
            left: "20px",
            bottom: "20px",
            fontWeight: "bold",
          }}
        >
          Detail
          <Typography
            component={"span"}
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              marginLeft: "10px",
            }}
          >
            <HiOutlineArrowNarrowRight />
          </Typography>
        </Link>
      </Grid>
    </Grid>
  );
};
