import { Box, ButtonBase, Grid, Typography } from "@mui/material";
import React from "react";
import "./index.scss";
import buttonBuy from "../../../assets/images/button_box.png";
import { useBuyBox } from "hooks/marketplace";
import GlobalModal from "components/organisms/GlobalModal/GlobalModal";
import CongratulationModal from "components/organisms/Modals/CongratulationModal/CongratulationModal";

interface BoxItemProps {
  title: String;
  img: any;
  circleImg: any;
  price: number;
}

export const BoxItem: React.FC<BoxItemProps> = ({
  title,
  img,
  price,
  circleImg,
}) => {
  const { buyAction } = useBuyBox("0.001");
  const handleClick = async () => {
    try {
      const draker = await buyAction();
      console.log("draker", draker);
      GlobalModal.show(
        <CongratulationModal newDraker={draker} onClickBtn={GlobalModal.hide} />
      );
    } catch (error) {}
  };
  return (
    <Grid
      sx={{
        width: "100%",
        height: "60%",
        position: "relative",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        flexDirection: "column",
        cursor: "pointer",
      }}
    >
      <Typography
        component={"p"}
        sx={{
          color: "transparent",
          fontSize: "35px",
          fontWeight: "bold",
          backgroundImage: `linear-gradient(to right,#cb9b51 22%, #f6e27a 45%,#f6f2c0 50%,#f6e27a 55%,#cb9b51 78%,#695832 100%)`,
          backgroundClip: "text",
        }}
      >
        {title}
      </Typography>
      <Box
        sx={{
          position: "relative",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          height: "400px",
        }}
      >
        <Box
          className="circle"
          component={"img"}
          src={circleImg}
          alt="box img"
          sx={{
            width: "400px",
            position: "absolute",
            zIndex: "1",
          }}
        />
        <Box
          component={"img"}
          src={img}
          alt="box img"
          sx={{ width: { xs: "300px", md: "300px" }, zIndex: 2 }}
        />
      </Box>
      <Typography
        component={"p"}
        sx={{ fontSize: "35px", color: "#fff", fontWeight: "bold" }}
      >
        ${price}
      </Typography>
      <ButtonBase
        onClick={handleClick}
        sx={{
          backgroundImage: `url(${buttonBuy})`,
          backgroundSize: "contain",
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          height: "50px",
          width: "150px",
          color: "#fff",
          fontSize: "20px",
          fontWeight: "bold",
        }}
      >
        Buy Now
      </ButtonBase>
    </Grid>
  );
};
