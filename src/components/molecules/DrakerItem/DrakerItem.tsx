import { ButtonBase, Grid, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { Spin } from "antd";
import { MARKETPLACE_STATUS } from "constants/marketplace";
import useMarketActionButton from "hooks/useMarketActionButton";
import React from "react";
import { getDrakerAddress } from "utils/addressHelpers";
import failImage from "../../../assets/images/fail_image.png";
import bnbIcon from "../../../assets/images/logo_bnb.png";
import "./index.scss";
import useFecthData from "hooks/useFetchData";
import useRefetch from "../../../zustand/useRefetch";
import drakerInfo from "config/drakerInfo";
import { Link } from "react-router-dom";
import bgBtn from "../../../assets/images/btn-bg.png";

export interface DrakerItemProps {
  id: string;
  type: string;
  drakerName?: string;
  imageURL: string;
  price: number;
  tokenId: string;
  listingId: string;
  owner: string;
  isInventory: boolean;
  paymentToken: string;
  drakerRarity: number;
}

export const DrakerItem: React.FC<DrakerItemProps> = ({
  id,
  type,
  drakerName,
  imageURL,
  price,
  tokenId,
  listingId,
  owner,
  isInventory,
  paymentToken,
  drakerRarity,
}) => {
  const { titleButton, action, isLoading } = useMarketActionButton({
    nftAddress: getDrakerAddress(),
    tokenId,
    listingId,
    seller: owner,
    owner,
    status: isInventory
      ? MARKETPLACE_STATUS.INVENTORY
      : MARKETPLACE_STATUS.ON_SALE,
    paymentToken,
    price,
  });

  const { refetch, changeRefetchStatus } = useRefetch();

  return (
    <Grid
      className="draker__item"
      sx={{
        position: "relative",
        backgroundColor: "#1c1f25",
        border: "1px solid #282c34",
        width: { xs: "220px", md: "170px" },
        borderRadius: "10px",
        overflow: "hidden",
        margin: "10px",
        padding: "15px",
        cursor: "pointer",
      }}
    >
      <Typography
        component={"p"}
        sx={{
          backgroundColor: drakerInfo.typeColor[type],
          width: "fit-content",
          padding: "1px 10px",
          borderRadius: "5px",
          color: "#fff",
          fontSize: "13px",
        }}
      >
        #{id.slice(0, 5).concat("...")}
      </Typography>
      <Typography
        component={"p"}
        sx={{
          color: "#fff",
          marginTop: "5px",
          display: "flex",
          alignItems: "center",
          fontSize: "10px",
        }}
      >
        <Box
          component="img"
          src={drakerInfo.typeIcon[type]}
          alt="icon"
          sx={{ width: "15px", marginRight: "5px" }}
        />
        Draker #{id.slice(0, 5).concat("...")}
      </Typography>
      <Link to={`detail/${tokenId}`}>
        <Box
          onError={({ currentTarget }) => {
            currentTarget.src = failImage;
          }}
          component={"img"}
          src={imageURL}
          alt="Draker image"
          sx={{ width: "100%" }}
        />
      </Link>
      <Box sx={{ width: "100%" }}>
        <Typography
          component={"p"}
          sx={{
            textAlign: "center",
            color: "#fff",
            fontSize: "14px",
          }}
        >
          Ξ {drakerRarity}
        </Typography>
        <Typography
          component={"p"}
          sx={{
            textAlign: "center",
            color: "#ababab",
            fontSize: "16px",
            fontWeight: "bold",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Box
            component={"img"}
            src={bnbIcon}
            alt="logo bnb"
            sx={{ width: "20px", marginRight: "5px" }}
          />
          {price}
        </Typography>
      </Box>
      <Box
        className="button__action"
        sx={{
          width: "100%",
          height: "50px",
          position: "absolute",
          left: "0",
          bottom: "-100px",
          transition: ".3s ease-in-out",
          borderBottomLeftRadius: "10px",
          borderBottomRightRadius: "10px",
          overflow: "hidden",
        }}
      >
        <ButtonBase
          onClick={async () => {
            await action();
            changeRefetchStatus(!refetch);
          }}
          sx={{
            width: "100%",
            height: "100%",
            position: "relative",
            backgroundColor: "#1c1f25",
            color: "#fff",
            fontSize: "25px",
            transition: ".3s ease-in-out",
          }}
        >
          {isLoading ? <Spin /> : titleButton}
        </ButtonBase>
      </Box>
    </Grid>
  );
};
