import { Box, ButtonBase } from "@mui/material";
import { Spin } from "antd";
import { MARKETPLACE_STATUS } from "constants/marketplace";
import useMarketActionButton from "hooks/useMarketActionButton";
import React from "react";
import { Link } from "react-router-dom";
import { getDrakerAddress } from "utils/addressHelpers";
import useRefetch from "../../../zustand/useRefetch";
import failImage from "../../../assets/images/fail_image.png";
import "./index.scss";

interface InventoryDrakerItemProps {
  drakerImage: string;
  tokenId: string;
  listingId: string;
  owner: string;
  isInventory: boolean;
  paymentToken: string;
}

export const InventoryDrakerItem: React.FC<InventoryDrakerItemProps> = ({
  drakerImage,
  tokenId,
  listingId,
  owner,
  isInventory,
  paymentToken,
}) => {
  const { titleButton, action, isLoading } = useMarketActionButton({
    nftAddress: getDrakerAddress(),
    tokenId,
    listingId,
    seller: owner,
    owner,
    status: isInventory
      ? MARKETPLACE_STATUS.INVENTORY
      : MARKETPLACE_STATUS.ON_SALE,
    paymentToken,
  });

  const { refetch, changeRefetchStatus } = useRefetch();

  if (isLoading) {
    if (isLoading == false) {
      changeRefetchStatus(!refetch);
    }
  }

  return (
    <Box className="inventory__draker">
      <Link to={`/detail/${tokenId}`}>
        <Box
          onError={({ currentTarget }) => {
            currentTarget.src = failImage;
          }}
          component={"img"}
          src={drakerImage}
          alt="draker"
          sx={{ width: "100%" }}
        />
      </Link>
      <ButtonBase
        className="button__action"
        onClick={action}
        sx={{
          width: "100%",
          position: "absolute",
          left: "0",
          bottom: "-100px",
          backgroundColor: "#adadad40",
          border: "1px solid #fff",
          borderBottomLeftRadius: "20px",
          borderBottomRightRadius: "20px",
          color: "#fff",
          fontSize: "25px",
          transition: ".3s ease-in-out",
        }}
      >
        {isLoading ? <Spin /> : titleButton}
      </ButtonBase>
    </Box>
  );
};
