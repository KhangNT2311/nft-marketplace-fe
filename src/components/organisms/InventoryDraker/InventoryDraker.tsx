import { Box, Grid, Pagination } from "@mui/material";
import { InventoryDrakerItem } from "components/molecules/InventoryDrakerItem/InventoryDrakerItem";
import React, { useEffect, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import useRefetch from "../../../zustand/useRefetch";
import useMarketplaceStore from "../../../zustand/marketplace/useMarketplaceStore";
import useTokenIdDraker from "../../../zustand/useTokenIdDraker";
const axios = require("axios");

interface InventoryDrakerProps {}

export const InventoryDraker: React.FC<InventoryDrakerProps> = ({}) => {
  const address = localStorage.getItem("account");
  const [drakerInventList, setDrakerInventList] = useState([]);
  const rebound = useMarketplaceStore((state: any) => state.rebound);
  const setMarketplaceState = useMarketplaceStore(
    (state: any) => state.setMarketplaceState
  );
  const isFirstRender = useRef(true);
  const [curerntPage, setCurerntPage] = useState(1);
  const [totalPage, setTotalPage] = useState(1);

  const { refetch, changeRefetchStatus } = useRefetch();

  const handleChangePage = (event, value) => {
    setCurerntPage(value);
  };

  useEffect(() => {
    if (rebound || isFirstRender.current) {
      const getInventory = async () => {
        await axios({
          method: "get",
          url: `https://nft-api.diviner.finance/v1/drakers?owner=${address}`,
          params: curerntPage,
          responseType: "stream",
        }).then(async function (response) {
          setDrakerInventList(response.data.data.results);
          setTotalPage(response.data.data.totalPages);
          setMarketplaceState({
            rebound: false,
          });
          console.log("response invent", response.data.data.totalPages);
        });
      };
      isFirstRender.current = false;
      getInventory();
    }
  }, [rebound, refetch]);
  console.log("drakerInventList", drakerInventList);
  return (
    <Grid sx={{ display: "flex", flexDirection: "column", height: "100%" }}>
      <Grid
        container
        spacing={3}
        sx={{
          width: "100%",
          height: "100%",
          overflow: "scroll",
          display: "flex",
          flexDirection: "row",
          flexWrap: "wrap",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        {drakerInventList &&
          drakerInventList.map((item, index) => {
            return (
              <Grid
                item
                xs={12}
                sm={5.5}
                md={3.5}
                key={index}
                sx={{
                  display: "flex",
                  position: "relative",
                  borderRadius: "20px",
                  margin: "10px",
                  height: "fit-content",
                  border: "1px solid #4c4c4c",
                  overflow: "hidden",
                  cursor: "pointer",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <InventoryDrakerItem {...item} isInventory={true} />
              </Grid>
            );
          })}
      </Grid>
      <Box
        sx={{ display: "flex", alignItems: "center", justifyContent: "center" }}
      >
        <Pagination
          className="pagination"
          count={totalPage}
          onChange={handleChangePage}
          variant="outlined"
          shape="rounded"
          sx={{ color: "#fff" }}
        />
      </Box>
    </Grid>
  );
};
