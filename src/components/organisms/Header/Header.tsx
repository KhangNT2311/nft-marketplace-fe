import { Box, ButtonBase, Grid, Typography } from "@mui/material";
import { ConnectWalletButton } from "components/atoms/ConnectWalletButton/ConnectWalletButton";
import { NavButton } from "components/atoms/NavButton/NavButton";
import { ethers } from "ethers";
import useAccount from "hooks/useAccount";
import useAuth from "hooks/useAuth";
import { BsHandbagFill } from "react-icons/bs";
import { formatAddress } from "utils/helpers";
import useUserStore from "../../../zustand/user/useUserStore";
import metamaskIcon from "../../../assets/images/logo_metamask.png";
import bnbIcon from "../../../assets/images/logo_bnb.png";
import { FiLogOut } from "react-icons/fi";

interface HeaderProps {
  title: String;
}

export const Header: React.FC<HeaderProps> = ({ title }) => {
  const account = useAccount();
  const { logout } = useAuth();
  const userTokenData = useUserStore((state: any) => state.userTokenData);
  return (
    <Grid
      sx={{
        position: "relative",
        display: "flex",
        alignItems: "center",
        justifyContent: { xs: "center", md: "space-between" },
        height: { xs: "60px", md: "100px" },
        borderBottom: "1px solid #282c34",
        padding: "0 20px",
      }}
    >
      <NavButton />
      <Typography
        component={"p"}
        sx={{
          color: "#c99681",
          fontSize: { xs: "27px", md: "37px" },
          fontWeight: "bold",
        }}
      >
        <Typography
          component={"span"}
          sx={{ color: "#fff", marginRight: "10px", fontSize: "20px" }}
        >
          <BsHandbagFill />
        </Typography>
        {title}
      </Typography>
      {account ? (
        <Grid
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "flex-start",
              marginRight: "10px",
            }}
          >
            <Box
              component={"img"}
              src={bnbIcon}
              alt="metamask icon"
              sx={{ width: "44px", marginRight: "10px" }}
            />
             <Box
              component={"img"}
              src={metamaskIcon}
              alt="metamask icon"
              sx={{ width: "44px", marginRight: "10px" }}
            />
            <Box
              sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "flex-start",
                color: "#5b6372",
              }}
            >
              <Typography component={"p"}>
                Your BNB: {ethers.utils.formatEther(userTokenData.bnb.balance)}
              </Typography>
              <Typography component={"p"}>
                Your Wallet: {formatAddress(account)}
              </Typography>
            </Box>
          </Box>
          <ButtonBase
            sx={{
              backgroundColor: "#3a3f50",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              color: "#13161b",
              fontSize: "30px",
              borderRadius: "50%",
              padding: "10px",
            }}
            className="header__button"
            type="submit"
            onClick={logout}
          >
            <FiLogOut />
          </ButtonBase>
        </Grid>
      ) : (
        <ConnectWalletButton />
      )}
    </Grid>
  );
};
