import { ButtonBase, Grid } from "@mui/material";
import React from "react";
import useErrorModalActive from "../../../zustand/useErrorModalActive";
import "./index.scss";

interface ModalLoginErrorProps {}

export const ModalLoginError: React.FC<ModalLoginErrorProps> = ({}) => {
  const { errorModalStatus, changeErrorModalStatus } = useErrorModalActive();

  return (
    <Grid
      className={errorModalStatus ? "" : "error__modal__inactive"}
      sx={{
        width: "300px",
        height: "200px",
        backgroundColor: "#fff",
        position: "fixed",
        left: "50%",
        top: "40px",
        zIndex: 10,
        borderRadius: "10px",
        padding: "10px",
        transition: ".3s ease-in-out",
        opacity: 1,
        visibility: "visible",
      }}
    >
      <Grid sx={{ width: "100%", height: "100%", position: "relative" }}>
        <ButtonBase sx={{position: 'absolute', right: '5px', top: '5px'}} onClick={()=>changeErrorModalStatus(!errorModalStatus)}>X</ButtonBase>
        Error
      </Grid>
    </Grid>
  );
};
