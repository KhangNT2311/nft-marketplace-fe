import { Grid, Pagination, Typography } from "@mui/material";
import { DrakerItem } from "components/molecules/DrakerItem/DrakerItem";
import { InventoryDrakerItem } from "components/molecules/InventoryDrakerItem/InventoryDrakerItem";
import useAccount from "hooks/useAccount";
import useFecthData from "hooks/useFetchData";
import React, { useState } from "react";

interface InventoryOnSaleProps {}

export const InventoryOnSale: React.FC<InventoryOnSaleProps> = ({}) => {
  const DOMAIN = process.env.REACT_APP_API_DOMAIN_FRONTEND;
  const account = useAccount();
  const MARKET_API = `${DOMAIN}/v1/market/listing-items?`;
  const [curerntPage, setCurerntPage] = useState(1);
  const { dataList, totalPage } = useFecthData(
    MARKET_API,
    curerntPage,
    6,
    "DRAKER",
    account
  );
  console.log("dataList", dataList);

  const handleChangePage = (event, value) => {
    setCurerntPage(value);
  };

  return (
    <Grid sx={{ height: "100%" }}>
      <Grid
        container
        sx={{
          width: "100%",
          height: "100%",
          overflow: "scroll",
          display: "flex",
          flexDirection: "row",
          flexWrap: "wrap",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        {dataList &&
          dataList.map((item, index) => {
            console.log("item", item);
            return (
              <Grid
                item
                md={3.5}
                key={index}
                sx={{
                  display: "flex",
                  position: "relative",
                  borderRadius: "20px",
                  margin: "10px",
                  height: "fit-content",
                  border: "1px solid #4c4c4c",
                  overflow: "hidden",
                  cursor: "pointer",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <InventoryDrakerItem
                  {...item}
                  tokenId={item.drakerInfo.tokenId}
                  drakerImage={item.drakerInfo.drakerImage}
                />
              </Grid>
            );
          })}
      </Grid>
      <Grid
        sx={{
          width: "100%",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Pagination
          className="pagination"
          count={totalPage}
          onChange={handleChangePage}
          variant="outlined"
          shape="rounded"
          sx={{ color: "#fff" }}
        />
      </Grid>
    </Grid>
  );
};
