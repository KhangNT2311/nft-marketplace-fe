import { Box, Grid } from "@mui/material";
import { NewsItem } from "components/molecules/NewsItem/NewsItem";
import React from "react";

interface NewsListProps {
  newsListData: any;
}

export const NewsList: React.FC<NewsListProps> = ({ newsListData }) => {
  console.log("new list data", newsListData);

  return (
    <Grid
      sx={{
        width: "100%",
        height: "auto",
        overflowY: "scroll",
        overflowX: "hidden",
        padding: "30px",
      }}
    >
      {newsListData &&
        newsListData.map((item, index) => {
          console.log(
            "🚀 ~ file: NewsList.tsx ~ line 17 ~ newsListData.articles&&newsListData.articles.map ~ item",
            item
          );
          return (
            <Box key={index} sx={{ marginBottom: "70px" }}>
              <NewsItem
                title={item.title}
                img={item.urlToImage}
                descriptiion={item.content}
                index={index}
              />
            </Box>
          );
        })}
    </Grid>
  );
};
