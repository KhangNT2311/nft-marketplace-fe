import { Grid, Link } from "@mui/material";
import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/navigation";

import { Autoplay, Navigation } from "swiper";

import '../../molecules/NewsItem/index.scss'

interface NewsBannerProps {
  bannerData: any;
}

export const NewsBanner: React.FC<NewsBannerProps> = ({ bannerData }) => {
  console.log("banner data", bannerData);

  return (
    <Grid>
      <Swiper
        slidesPerView={1}
        spaceBetween={30}
        loop={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        navigation={true}
        modules={[Autoplay, Navigation]}
        className="mySwiper"
      >
        {bannerData &&
          bannerData.map((item, index) => {
            return (
              <SwiperSlide key={index}>
                <Grid
                  sx={{
                    width: "100%",
                    height: { xs: "300px", md: "500px", xl: "700px" },
                    backgroundImage: `url(${item.urlToImage})`,
                    backgroundSize: "cover",
                    position: "relative",
                    backgroundPosition: "center",
                  }}
                >
                  <Link
                    className="news__title"
                    href={item.url}
                    sx={{
                      height: "120px",
                      position: "absolute",
                      bottom: "5%",
                      left: "5%",
                      color: "#00c7ff",
                      textDecoration: "none",
                      fontSize: "40px",
                      fontWeight: "bold",
                      pading: "0 30px 0 0",
                      textOverflow: "ellipsis",
                      overflow: "hidden",
                    }}
                  >
                    {item.title}
                  </Link>
                </Grid>
              </SwiperSlide>
            );
          })}
      </Swiper>
    </Grid>
  );
};
