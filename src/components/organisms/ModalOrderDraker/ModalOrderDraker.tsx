import {
  Box,
  Button,
  ButtonBase,
  Grid,
  Tooltip,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { Formik, Field, Form } from "formik";
import drakerInfo from "config/drakerInfo";
import useModalOrderVisible from "../../../zustand/useModalOrderVisible";
import "./index.scss";
import { join } from "path/posix";
import useGetDerakerPart from "hooks/useGetDerakerPart";
import useGetDrakerImage from "hooks/useGetDrakerImage";

interface ModalOrderDrakerProps {}

export const ModalOrderDraker: React.FC<ModalOrderDrakerProps> = ({}) => {
  const getPartApi = `${process.env.REACT_APP_API_DOMAIN_FRONTEND}/v1/parts`;
  const getDrakerImageApi = `${process.env.REACT_APP_API_DOMAIN_FRONTEND}/v1/draker-images`;
  const { modalOrderStatus, changeModalOrderStatus } = useModalOrderVisible();
  const [beast, setBeast] = useState(null);
  const [tail, setTail] = useState(null);
  const [body, setBody] = useState(null);
  const urlImage = "https://booking-app-bucket-ntk.s3.us-east-2.amazonaws.com";

  const { partList } = useGetDerakerPart(getPartApi);
  const { drakerImage } = useGetDrakerImage(
    getDrakerImageApi,
    beast,
    tail,
    body
  );

  console.log(
    "🚀 ~ file: ModalOrderDraker.tsx ~ line 31 ~ drakerImage",
    drakerImage
  );
  return (
    <Grid
      className={modalOrderStatus ? "" : "modal__hidden"}
      sx={{
        display: "flex",
        width: "100vw",
        height: "100vh",
        alignItems: "center",
        justifyContent: "center",
        position: "fixed",
        top: 0,
        left: 0,
        backgroundColor: "#000000c2",
        zIndex: "10000000",
      }}
    >
      <Grid
        className="modal__order"
        sx={{
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          width: "500px",
          backgroundColor: "#383838a3",
          position: "relative",
          padding: "20px",
          borderRadius: "10px",
          overflow: "hidden",
        }}
      >
        <Box sx={{ zIndex: 100 }}>
          <ButtonBase
            onClick={() => changeModalOrderStatus(!modalOrderStatus)}
            sx={{
              width: "40px",
              height: "40px",
              borderRadius: "50%",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              position: "absolute",
              top: "5px",
              right: "5px",
              backgroundColor: "#6a6a6a",
              color: "#fff",
              fontSize: "25px",
            }}
          >
            X
          </ButtonBase>
          <Typography
            component={"p"}
            sx={{
              fontSize: "20px",
              fontWeight: "bold",
              marginBottom: "40px",
              color: "#fff",
              textAlign: "center",
            }}
          >
            Select Part You Want
          </Typography>
          <Formik
            initialValues={{
              beast: beast,
              tail: tail,
              body: body,
            }}
            onSubmit={async (values) => {
              await new Promise((r) => setTimeout(r, 500));
              alert(JSON.stringify(values, null, 2));
            }}
          >
            <Form>
              <Grid sx={{ marginBottom: "10px" }}>
                {partList
                  .filter((part) => part.type == "beast")
                  .map((item, index) => {
                    console.log("🚀 ~ file: ModalOrderDraker.tsx ~ line 119 ~ .map ~ item", item)
                    return (
                      <ButtonBase
                        key={index}                                 
                        className={beast == item.id ? "part__active" : ""}
                        onClick={() => {
                          setBeast(item.id);
                        }}
                        sx={{
                          width: "80px",
                          height: "80px",
                          padding: "5px 10px",
                          margin: "10px",
                          backgroundColor: "#1c1f25",
                          color: "#fff",
                          borderRadius: "50%",
                        }}
                      >
                        <Box
                          component={"img"}
                          src={item.imageUrl}
                          alt="beast"
                          sx={{ width: "32px" }}
                        />
                      </ButtonBase>
                    );
                  })}
              </Grid>
              <Grid sx={{ marginBottom: "10px" }}>
                {partList
                  .filter((part) => part.type == "tail")
                  .map((item, index) => {
                    return (
                      <ButtonBase
                        key={index}
                        className={tail == item.id ? "part__active" : ""}
                        onClick={() => setTail(item.id)}
                        sx={{
                          width: "80px",
                          height: "80px",
                          padding: "5px 10px",
                          margin: "10px",
                          backgroundColor: "#1c1f25",
                          color: "#fff",
                          borderRadius: "50%",
                        }}
                      >
                        <Box
                          component={"img"}
                          src={item.imageUrl}
                          alt="tail"
                          sx={{ width: "32px" }}
                        />
                      </ButtonBase>
                    );
                  })}
              </Grid>
              <Grid>
                {partList
                  .filter((part) => part.type == "body")
                  .map((item, index) => {
                    return (
                      <ButtonBase
                        key={index}
                        className={body == item.id ? "part__active" : ""}
                        onClick={() => setBody(item.id)}
                        sx={{
                          width: "80px",
                          height: "80px",
                          padding: "5px 10px",
                          margin: "10px",
                          backgroundColor: "#1c1f25",
                          color: "#fff",
                          borderRadius: "50%",
                        }}
                      >
                        <Box
                          component={"img"}
                          src={item.imageUrl}
                          alt="body"
                          sx={{ width: "32px" }}
                        />
                      </ButtonBase>
                    );
                  })}
              </Grid>
              <Grid>
                <Typography component={"p"} sx={{ color: "#fff" }}>
                  Preview:
                </Typography>
                {beast != null &&
                  tail != null &&
                  body != null &&
                  drakerImage && (
                    <Box
                      component={"img"}
                      src={drakerImage}
                      alt="full body"
                      sx={{ width: "150px" }}
                    />
                  )}
              </Grid>
              <ButtonBase
                type="submit"
                sx={{
                  width: "100%",
                  backgroundColor: "#0057a3",
                  padding: "7px",
                  fontSize: "20px",
                  color: "#fff",
                  borderRadius: "10px",
                }}
              >
                Submit
              </ButtonBase>
            </Form>
          </Formik>
        </Box>
      </Grid>
    </Grid>
  );
};
