import styled from "styled-components";

function CongratulationModal({
  newDraker = {},
  titleBtn = "Close",
  onClickBtn,
}: any) {
  const { attributeElements } = newDraker;
  const listAtt = [
    {
      img: "eff",
      content: "Attack",
      att: attributeElements?.attack,
    },
    {
      img: "comfort",
      content: "Defend",
      att: attributeElements?.defend,
    },
    {
      img: "res",
      content: "Speed",
      att: attributeElements?.speed,
    },
  ];

  return (
    <CongratulationStyled className={"hide__show__box"}>
      <div className="title__congratulation">
        <h3>Congratulation</h3>
        <p>
          You've received{" "}
          {newDraker?.rarity === "Epic" || newDraker?.rarity === "Uncommon" ? (
            <>an {newDraker?.rarity}</>
          ) : (
            <>a {newDraker?.rarity}</>
          )}{" "}
          Draker!{" "}
        </p>
      </div>
      <div className="img__congratulation">
        <img className="box" src={newDraker.drakerImage} alt="" />
        <img
          className="light"
          src="./images/mystery/light-congratulation.png"
          alt=""
        />
      </div>
      <div className="att__show">
        {listAtt.map((item, index) => (
          <div className="item" key={index}>
            <img src={`images/modal-market/${item.img}.svg`} alt="icon" />
            <p>
              <span className="item__title">{item.content}</span>
              <span className="item__content">{item.att}</span>
            </p>
          </div>
        ))}
      </div>

      <div className="button__sell" style={{ width: "95%", margin: "0 auto" }}>
        <button onClick={onClickBtn}>{titleBtn}</button>
      </div>
    </CongratulationStyled>
  );
}

const CongratulationStyled = styled.div`
  background: #001919;
  border-radius: 16px;
  padding: 20px 50px;
  position: relative;
  @media (max-width: 500px) {
    padding: 20px 25px;
  }
  &.show__box {
    width: 380px;
    border: 2px solid #0fc1a9;
  }

  .title__congratulation {
    text-align: center;
    color: #fff;
    font-size: 16px;
    font-weight: 500;
    p {
      margin: 0px;
    }
    h3 {
      font-weight: 800;
      font-size: 24px;
      color: #fff;
      margin: 0;
      text-align: center;
    }
  }
  .img__congratulation {
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 30px 0px;
    position: relative;

    @keyframes rotating {
      to {
        transform: rotate(360deg);
      }
    }

    .box {
      z-index: 2;
      width: 270px;
    }
    .light {
      position: absolute;
      animation: rotating 5s linear infinite;
    }
  }
  .att__show {
    /* display: flex; */
    /* justify-content: space-between; */
    padding: 20px 0px;
    grid-template-columns: repeat(2, 1fr);
    display: grid;
    gap: 0px 10px;

    .item {
      &:nth-child(even) {
        justify-content: flex-end;
      }
      &:nth-child(odd) {
        justify-content: flex-start;
      }
      > p {
        display: flex;
      }
      &__title {
        @media screen and (min-width: 576px) {
          width: 100px;
          display: block;
        }
      }
      display: flex;
      align-items: flex-start;

      @media (max-width: 500px) {
        margin-right: 0px;
        justify-content: left;
      }
      &:last-child {
        margin-right: 0px;
      }
      p {
        margin: 0px;
        span {
          font-size: 16px;
          font-weight: 500;
          padding: 0px 5px;
          color: #99a3a3;
        }
        .item__content {
          padding: 0px;
          align-items: baseline;
          font-size: 20px;
          font-weight: 600;
          color: #fff;
          line-height: 30px;
        }
      }
      img {
        width: 20px;
        margin-top: 5px;
      }
    }
  }
`;
export default CongratulationModal;
