import { Grid } from "@mui/material";
import React from "react";
import { DrakerList } from "../DrakerList/DrakerList";
import { FilterBox } from "../FilterBox/FilterBox";
import { ModalOrderDraker } from "../ModalOrderDraker/ModalOrderDraker";

interface MarketProps {}

export const Market: React.FC<MarketProps> = ({}) => {
  return (
    <Grid sx={{ height: "calc(100vh - 100px)" }}>
      <Grid container>
        <Grid item xs={12} md={4} lg={3} xl={2}>
          <FilterBox />
        </Grid>
        <Grid item xs={12} md={8} lg={9} xl={10}>
          <DrakerList />
        </Grid>
      </Grid>
    </Grid>
  );
};
