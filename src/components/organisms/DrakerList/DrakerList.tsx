import { CircularProgress, Grid, Pagination } from "@mui/material";
import { Box } from "@mui/system";
import {
  DrakerItem,
  DrakerItemProps,
} from "components/molecules/DrakerItem/DrakerItem";
import React, { useEffect, useState } from "react";
import { getDrakerContract } from "utils/contractHelpers";
import useMarketplaceStore from "../../../zustand/marketplace/useMarketplaceStore";
import useFecthData from "hooks/useFetchData";
import "./index.scss";

interface DrakerListProps {}

export const DrakerList: React.FC<DrakerListProps> = ({}) => {
  const DOMAIN_API = process.env.REACT_APP_API_DOMAIN_FRONTEND;
  const [typeColor, setTypeColor] = useState("");
  const [curerntPage, setCurerntPage] = useState(1);
  const setMarketplaceState = useMarketplaceStore(
    (state: any) => state.setMarketplaceState
  );
  const rebound = useMarketplaceStore((state: any) => state.rebound);

  const MARKET_API = `${DOMAIN_API}/v1/market/listing-items?`;
  const { dataList, totalPage } = useFecthData(
    MARKET_API,
    curerntPage,
    24,
    "DRAKER"
  );

  const marketList = dataList;

  const handleChangePage = (event, value) => {
    setCurerntPage(value);
  };

  return (
    <Grid
      sx={{
        height: {
          xs: "calc(100vh - 60px - 60px)",
          md: "calc(100vh - 60px - 100px)",
        },
        overflowY: "scroll",
      }}
    >
      <Grid
        sx={{
          display: "flex",
          flexWrap: "wrap",
          alignItems: "flex-start",
          alignContent: "flex-start",
          justifyContent: "center",
        }}
      >
        {marketList ? (
          marketList.map((item, index) => {
            return (
              <Box key={index}>
                {item.drakerInfo.drakerImage ? (
                  <DrakerItem
                    {...item}
                    {...item.drakerInfo}
                    id={item.draker}
                    imageURL={item.drakerInfo.drakerImage}
                    price={item.price}
                    type={item.drakerInfo.drakerClass}
                    isInventory={false}
                    owner={item.owner}
                    tokenId={item.drakerInfo.tokenId}
                    drakerRarity={item.drakerInfo.drakerRarity}
                  />
                ) : (
                  <CircularProgress color="success" />
                )}
              </Box>
            );
          })
        ) : (
          <CircularProgress color="success" />
        )}
      </Grid>
      <Box
        sx={{
          width: "100%",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          marginTop: "20px",
        }}
      >
        <Pagination
          className="pagination"
          count={totalPage}
          onChange={handleChangePage}
          variant="outlined"
          shape="rounded"
          sx={{ color: "#fff" }}
        />
      </Box>
    </Grid>
  );
};
