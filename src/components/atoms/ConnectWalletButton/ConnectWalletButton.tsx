import {
  connectorLocalStorageKey,
  ConnectorNames,
} from "@diviner-protocol/uikit";
import { Box, ButtonBase, Grid } from "@mui/material";
import { ethers } from "ethers";
import { useActiveWeb3React } from "hooks";
import useAuth from "hooks/useAuth";
import React, { useState } from "react";
import metamaskIcon from "../../../../public/images/connect/metamask.png";
const axios = require("axios");

interface ConnectWalletButtonProps {}

const metamaskButton = {
  title: "Metamask",
  icon: "metamask.png",
  connectorId: ConnectorNames.Injected,
};

export const ConnectWalletButton: React.FC<ConnectWalletButtonProps> = ({}) => {
  const DOMAIN = process.env.REACT_APP_API_DOMAIN_FRONTEND;
  const { login } = useAuth();
  const { account } = useActiveWeb3React();
  const [message, setMessage] = useState("");
  const [signature, setSignature] = useState("");
  const { ethereum } = window;
  const handleLogin = async () => {
    // const newAccounts = await ethereum.request({
    //   method: 'eth_accounts',
    // })
    await login(metamaskButton.connectorId);
    const provider = new ethers.providers.Web3Provider(window.ethereum, "any");
    const signer = provider.getSigner();
    const account = await signer.getAddress();
    const signature = await getMessage(signer, account);
    await getAccessToken(signature, account);
  };

  const getMessage = async (signer, account) => {
    const response = await axios({
      method: "get",
      url: `${DOMAIN}/v1/auth/get-message/${account}`,
      responseType: "stream",
    });
    const message = response.data.message;
    // setMessage(response.data.message);
    const signature = await signer.signMessage(message);
    return signature;
  };

  const getAccessToken = (signature, account) => {
    axios({
      method: "post",
      url: `http://localhost:4023/v1/auth/connect-wallet`,
      responseType: "stream",
      data: {
        id: account,
        signature,
      },
    }).then(function (response) {
      localStorage.setItem("accessToken", response.data.tokens.access.token);
    });
  };

  return (
    <Grid>
      <ButtonBase
        sx={{
          padding: "5px 25px",
          backgroundColor: "#046cfc",
          borderRadius: "5px",
          fontSize: "17px",
          color: "#fff",
          //   fontWeight: "bold",
        }}
        onClick={() => {
          handleLogin();
        }}
      >
        <Box
          component={"img"}
          src={`/images/connect/${metamaskButton.icon}`}
          alt={metamaskButton.title}
          sx={{ width: "40px", marginRight: "10px" }}
        />
        Login with {metamaskButton.title}
      </ButtonBase>
    </Grid>
  );
};
