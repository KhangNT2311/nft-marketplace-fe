import { ConnectorNames } from "@diviner-protocol/uikit";
import { useWeb3React } from "@web3-react/core";
import { useActiveWeb3React } from "hooks";
import useAccount from "hooks/useAccount";
import useAuth from "hooks/useAuth";
import { Detail } from "pages/Detail";
import { Home } from "pages/Home";
import { Inventory } from "pages/Inventory";
import { MysteryBox } from "pages/MysteryBox";
import { useEffect } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import useUserStore from "./zustand/user/useUserStore";
import 'antd/dist/antd.css';
import { News } from "pages/News";

function App() {
  const account = useAccount();
  const { login } = useAuth();
  const fetchApprovalNft = useUserStore(
    (state: any) => state.fetchApprovalNftMarket
  );
  const fetchUserTokenData = useUserStore(
    (state: any) => state.fetchUserTokenData
  );
  useEffect(() => {
    if (account) {
      localStorage.setItem("account", account);
    }
  }, [account]);
  useEffect(() => {
    // dispatch(fetchPrices());
    if (account) {
      login(ConnectorNames.Injected);
      fetchApprovalNft(account);
      fetchUserTokenData(account);
    }
  }, [account]);
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<Home />} path="/" />
        <Route element={<Inventory />} path="/inventory" />
        <Route element={<MysteryBox />} path="/mystery-box" />
        <Route element={<Detail />} path="/detail/:tokenId" />
        <Route element={<News />} path="/news" />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
