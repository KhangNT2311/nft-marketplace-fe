import React, { useEffect, useState } from "react";
import useRefetch from "../zustand/useRefetch";
import useMarketplaceStore from "../zustand/marketplace/useMarketplaceStore";
const axios = require("axios");

export default function useFecthData(
  api: string,
  page?: number,
  limit?: number,
  marketType?: string,
  owner?: string
) {
  const [loading, setLoading] = useState(true);
  const [hasMoreNews, setHasMoreNews] = useState(false);
  const [dataList, setDataList] = useState([]);
  const [totalPage, setTotalPage] = useState(0);
  const setMarketplaceState = useMarketplaceStore(
    (state: any) => state.setMarketplaceState
  );
  const { refetch, changeRefetchStatus } = useRefetch();

  let cancel;
  const resData = [];

  useEffect(() => {
    const getNewsList = async () => {
      await axios({
        method: "get",
        url: `${api}`,
        params: { limit, page, marketType: marketType, owner },
        cancelToken: new axios.CancelToken((c) => (cancel = c)),
      }).then(function (response) {
        console.log(
          "🚀 ~ file: useFetchData.ts ~ line 32 ~ getNewsList ~ response",
          response.data.data.results
        );
        setTotalPage(response.data.data.totalPages);
        setDataList(response.data.data.results);
        setLoading(false);
      });
    };

    getNewsList();
  }, [page, refetch]);

  return {
    dataList,
    setDataList,
    loading,
    hasMoreNews,
    totalPage,
  };
}
