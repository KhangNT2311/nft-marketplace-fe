import { useWeb3React } from "@web3-react/core";
import { notification } from "antd";
import GlobalModal from "components/organisms/GlobalModal/GlobalModal";
import { BigNumber, ethers, utils } from "ethers";
import { useCallback, useState } from "react";
import { calculateGasMargin } from "utils";
import useRefetch from "../zustand/useRefetch";
import useMarketplaceStore from "../zustand/marketplace/useMarketplaceStore";
import useUserStore from "../zustand/user/useUserStore";
import { useMarketplaceContract, useSaleContract } from "./useContract";
import axios from "axios";
const DENY_TX_MESSAGE =
  "MetaMask Tx Signature: User denied transaction signature.";
// const url = process.env.REACT_APP_API_DOMAIN_FRONTEND;
const url = "http://localhost:4055";
const getBuyBoxEvent = async (params = {}) => {
  const response = await axios({
    url: `${url}/v1/listen-transaction/buy-box`,
    params,
    method: "GET",
  });
  return response;
};
// const getBuyBoxEvent = async (params = {}) => {
//   const response = await API({
//     url: `${process.env.REACT_APP_API_DOMAIN_FRONTEND}/v1/buy-box`,
//     params,
//     method: "GET",
//   });
//   return response;
// };
export const useBuyBox = (price: string) => {
  const { account } = useWeb3React();
  const saleContract = useSaleContract();
  const [isLoading, setIsLoading] = useState(false);
  const setUserState = useUserStore((state: any) => state.setUserStore);

  const buyAction = useCallback(async () => {
    try {
      setIsLoading(true);

      const option = { value: ethers.utils.parseEther(price) };

      const response = await saleContract.estimateGas
        .buyMysteryBox(option)
        .catch(() => {
          return saleContract.estimateGas.buyMysteryBox(option);
        });

      const txHash = await saleContract.buyMysteryBox({
        ...option,
        gasLimit: calculateGasMargin(response),
      });
      console.log("txHash", txHash);

      // await txHash.wait();
      const results = await getBuyBoxEvent({
        transactionId: txHash.hash,
      });

      notification.success({
        message: "Purchase Box successfully",
        placement: "topRight",
      });
      console.log("results", results.data);
      // setUserState({
      //   rebound: true,
      // });
      return results.data;
    } catch (error) {
      console.log("error buy action", error);
      throw error;
    } finally {
      setIsLoading(false);
    }
  }, [saleContract, account]);

  return { isLoading, buyAction };
};

// returns a variable indicating the state of the approval and a function which approves if necessary or early returns
export function useCancelListing(listingId) {
  const { refetch, changeRefetchStatus } = useRefetch();
  const marketplaceContract = useMarketplaceContract();
  const setUserState = useUserStore((state: any) => state.setUserStore);
  const setMarketplaceState = useMarketplaceStore(
    (state: any) => state.setMarketplaceState
  );
  const fetching = useMarketplaceStore((state: any) => state.fetching);

  const onCancelListing = useCallback(async () => {
    // const callData = getCallData.cancelListing({ tokenId, nftAddress: characterAddress })
    // dispatch(addTransaction({ callData }))

    try {
      setMarketplaceState({
        fetching: {
          ...fetching,
          button: true,
        },
      });
      const estimatedGas = await marketplaceContract.estimateGas.cancelListing(
        listingId
      );

      const tx = await marketplaceContract.cancelListing(listingId, {
        gasLimit: calculateGasMargin(estimatedGas),
      });

      const wait = await tx.wait();

      notification.success({
        message: "Cancel sell successfully",
      });
      setMarketplaceState({
        rebound: true,
      });

      changeRefetchStatus(!refetch);
    } catch (error) {
      console.error("Cancel listing error", error);
      if ((error as any).message === DENY_TX_MESSAGE) {
        // dispatch(clearTransactionNotConfirmed({ callData }))
      } else {
        // dispatch(errorTransaction({ callData, message: 'Cancel listing fail' }))
        // dispatch(clearTransaction({ callData }))
      }
      throw error;
    } finally {
      setMarketplaceState({
        isLoading: false,
        rebound: true,
        fetching: {
          ...fetching,
          button: false,
        },
      });
    }
  }, [marketplaceContract, listingId]);

  return [onCancelListing];
}

// returns a variable indicating the state of the approval and a function which approves if necessary or early returns
export function usePurchaseListing(
  characterAddress,
  listingId,
  paymentToken,
  price
) {
  const { refetch, changeRefetchStatus } = useRefetch();
  const setMarketplaceState = useMarketplaceStore(
    (state: any) => state.setMarketplaceState
  );
  const setUserState = useUserStore((state: any) => state.setUserStore);
  const fetching = useMarketplaceStore((state: any) => state.fetching);
  const marketplaceContract = useMarketplaceContract();
  const onPurchaseListing = useCallback(async () => {
    let msgValue = ethers.utils.parseEther("0");

    if (paymentToken === ethers.constants.AddressZero) {
      msgValue = ethers.utils.parseEther(price.toString());
    }

    try {
      setMarketplaceState({
        fetching: {
          ...fetching,
          button: true,
        },
      });
      const estimatedGas =
        await marketplaceContract.estimateGas.purchaseListing(listingId, {
          value: msgValue,
        });
      const tx = await marketplaceContract.purchaseListing(listingId, {
        gasLimit: calculateGasMargin(estimatedGas),
        value: msgValue,
      });
      setMarketplaceState({
        isLoading: true,
      });

      const wait = await tx.wait();
      notification.success({
        message: "Purchase successfully",
      });

      setUserState({
        rebound: true,
      });

      changeRefetchStatus(!refetch);
    } catch (error) {
      if ((error as any).message === DENY_TX_MESSAGE) {
        // dispatch(clearTransactionNotConfirmed({ callData }))
      } else {
        // dispatch(errorTransaction({ callData, message: 'Purchase listing fail' }))
        // dispatch(clearTransaction({ callData }))
      }
      throw error;
    } finally {
      setMarketplaceState({
        isLoading: false,
        rebound: true,
        fetching: {
          ...fetching,
          button: false,
        },
      });
    }
  }, [marketplaceContract, characterAddress, listingId]);

  return [onPurchaseListing];
}

export function useAddListing(tokenId, characterAddress) {
  const marketplaceContract = useMarketplaceContract();

  const { refetch, changeRefetchStatus } = useRefetch();
  const { account } = useWeb3React();
  const setMarketplaceState = useMarketplaceStore(
    (state: any) => state.setMarketplaceState
  );
  const addListing = useCallback(
    async (amount, price, paymentToken) => {
      try {
        setMarketplaceState({
          fetching: {
            button: true,
          },
        });
        console.log({ price, amount, paymentToken, tokenId, characterAddress });
        if (!price) return;
        let msgValue = ethers.utils.parseEther("0");
        console.log("msgValue", msgValue);

        const priceAfterFormatEther = utils.parseEther(price?.toString());
        if (paymentToken === ethers.constants.AddressZero) {
          msgValue = priceAfterFormatEther
            .mul(ethers.BigNumber.from("0"))
            .div(ethers.BigNumber.from("100"));
        }
        console.log("estimated gas");

        const estimatedGas = await marketplaceContract.estimateGas.addListing(
          tokenId,
          amount,
          priceAfterFormatEther,
          paymentToken,
          characterAddress,
          { value: msgValue }
        );

        console.log("estimated 1", estimatedGas);

        const tx = await marketplaceContract.addListing(
          tokenId,
          amount,
          priceAfterFormatEther,
          paymentToken,
          characterAddress,
          {
            gasLimit: calculateGasMargin(estimatedGas),
            value: msgValue,
          }
        );

        const wait = await tx.wait();
        notification.success({
          message: "Sell successfully",
        });

        setMarketplaceState({
          rebound: true,
        });

        changeRefetchStatus(!refetch);
      } catch (error) {
        console.error("Sell failed", error);
        if ((error as any).message === DENY_TX_MESSAGE) {
        } else {
        }
        throw error;
      } finally {
        setMarketplaceState({
          isLoading: false,
          rebound: true,
          fetching: {
            button: false,
          },
        });
      }
    },
    [tokenId, characterAddress, marketplaceContract]
  );

  return [addListing];
}
