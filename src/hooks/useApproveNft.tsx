import { useCallback } from "react";
import { calculateGasMargin } from "utils";
import useMarketplaceStore from "../zustand/marketplace/useMarketplaceStore";
import useUserStore from "../zustand/user/useUserStore";
import { useERC721 } from "./useContract";

// returns a variable indicating the state of the approval and a function which approves if necessary or early returns
export function useApproveForAll(
  nftAddress?: string,
  addressNeedApprove?: string
) {
  const nftContract = useERC721(nftAddress);
  const fetching = useMarketplaceStore((state: any) => state.fetching);
  const setUserState = useUserStore((state: any) => state.setUserStore);
  const setMarketplaceState = useMarketplaceStore(
    (state: any) => state.setMarketplaceState
  );
  const onApproveForAll = useCallback(async () => {
    setMarketplaceState({
      fetching: {
        ...fetching,
        button: true,
      },
    });
    const estimatedGas = await nftContract.estimateGas
      .setApprovalForAll(addressNeedApprove, true)
      .catch(() => {
        return nftContract.estimateGas.setApprovalForAll(
          addressNeedApprove,
          true
        );
      });

    try {
      const tx = await nftContract.setApprovalForAll(addressNeedApprove, true, {
        gasLimit: calculateGasMargin(estimatedGas),
      });
      setMarketplaceState({
        isLoading: true,
      });
      const response = await tx.wait();
      console.log("resonse", response);

      setUserState({ isApprovedForAll: true });
    } catch (error) {
      console.log("err approve nft", error);
    } finally {
      setMarketplaceState({
        isLoading: false,
        fetching: {
          ...fetching,
          button: false,
        },
      });
    }
  }, [nftContract, addressNeedApprove]);

  return [onApproveForAll];
}
