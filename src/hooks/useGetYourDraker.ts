import React, { useEffect, useState } from "react";
const axios = require("axios");

export default function useGetYourDraker(api: string) {
  const [draker, setDraker] = useState(null);

  useEffect(() => {
    const getDrakerDetail = async () => {
      await axios({
        method: "get",
        url: api,
        params: {},
      })
        .then(function (response) {
          console.log("Draker Data", response.data.data);
          setDraker(response.data.data);
        })
        .catch(function (error) {
          console.log(error);
        });
    };
    getDrakerDetail();
  }, []);

  return {
    draker,
  };
}
