import { useWeb3React } from "@web3-react/core";
import GlobalModal from "components/organisms/GlobalModal/GlobalModal";
import SellModal from "components/organisms/Modals/SellModal";
// eslint-disable-next-line
import {
  getBusdAddress,
  getDrakerAddress,
  getDrtAddress,
  getMarketplaceAddress,
} from "utils/addressHelpers";
import useRefetch from "../zustand/useRefetch";
import useMarketplaceStore from "../zustand/marketplace/useMarketplaceStore";
import useUserStore from "../zustand/user/useUserStore";
// import SellModal from 'views/Modal/SellModal'
import { useCancelListing, usePurchaseListing } from "./marketplace";
import { useApproveCallbackCustom } from "./useApprove";
import { useApproveForAll } from "./useApproveNft";

const useMarketActionButton = ({
  nftAddress = getDrakerAddress(),
  status,
  tokenId = "",
  seller,
  owner = "",
  listingId = "",
  paymentToken = "",
  price = 0,
}) => {
  const { account, chainId } = useWeb3React();
  const { busd, drt } = useUserStore((state: any) => state.userTokenData);
  const fetching = useMarketplaceStore((state: any) => state.fetching);
  const isApprovedForAll = useUserStore((state: any) => state.isApprovalForAll);
  const [onCancelListing] = useCancelListing(listingId);
  const [onPurchaseListing] = usePurchaseListing(
    nftAddress,
    listingId,
    paymentToken,
    price
  );
  const [onApproveNFT] = useApproveForAll(nftAddress, getMarketplaceAddress());

  const [onApproveBUSD] = useApproveCallbackCustom(
    getBusdAddress(),
    getMarketplaceAddress()
  );

  const [onApproveEMV] = useApproveCallbackCustom(
    getDrtAddress(),
    getMarketplaceAddress()
  );



  let titleButton = null;
  let action = null;
  // console.log(
  //   "paymentToken === getBusdAddress()",
  //   paymentToken,
  //   getBusdAddress()
  // );
  if (!account) {
    titleButton = "Connect Wallet";
    // action = () => {
    //   GlobalModal.show(<ConnectWalletModal onClose={GlobalModal.hide} />);
    // };
  } else if (status) {
    if (seller === account) {
      titleButton = "Cancel";
      action = onCancelListing;
    } else if (paymentToken === getBusdAddress() && !busd.isAllowance) {
      titleButton = "Approve BUSD";
      action = onApproveBUSD;
    } else if (paymentToken === getDrtAddress() && !drt.isAllowance) {
      titleButton = "Approve DRT";
      action = onApproveEMV;
    } else {
      titleButton = "Buy";
      action = onPurchaseListing;
    }
  } else if (!isApprovedForAll) {
    titleButton = "Approve NFT";
    action = onApproveNFT;
  } else if (owner.toLowerCase() === account.toLowerCase()) {
    titleButton = "Sell";
    action = () =>
      GlobalModal.show(
        <SellModal
          onClose={GlobalModal.hide}
          nftAddress={nftAddress}
          tokenId={tokenId}
          paymentToken={paymentToken}
        />
      );
  } else {
    titleButton = null;
  }

  return {
    titleButton,
    action,
    isLoading: fetching.button,
  };
};

export default useMarketActionButton;
