import React, { useEffect, useState } from "react";
import useRefetch from "../zustand/useRefetch";
import useMarketplaceStore from "../zustand/marketplace/useMarketplaceStore";
const axios = require("axios");

export default function useGetDrakerDetail(api: string, tokenId: string) {
  const [draker, setDraker] = useState(null);

  useEffect(() => {
    const getDrakerDetail = async () => {
      await axios({
        method: "get",
        url: api,
        params: { tokenId },
      })
        .then(function (response) {
          console.log("Draker Data", response.data.data);
          setDraker(response.data.data);
        })
        .catch(function (error) {
          console.log(error);
        });
    };
    getDrakerDetail();
  }, [tokenId]);

  return {
    draker,
  };
}
