import React, { useEffect, useState } from "react";
import useRefetch from "../zustand/useRefetch";
import useMarketplaceStore from "../zustand/marketplace/useMarketplaceStore";
const axios = require("axios");

export default function useGetDrakerImage(
  api: string,
  beastId: string,
  tailId: string,
  bodyId: string
) {
  const [loading, setLoading] = useState(true);
  const [drakerImage, setDrakerImage] = useState("");
  const { refetch, changeRefetchStatus } = useRefetch();

  useEffect(() => {
    const getDrakerImage = async () => {
      await axios({
        method: "get",
        url: `${api}`,
        params: { parts: [tailId, beastId, bodyId] },
      }).then(function (response) {
        setDrakerImage(response.data.data.results[0].imageUrl);
        setLoading(false);
      });
    };

    if (beastId && tailId && bodyId) {
      getDrakerImage();
    }
  }, [refetch, beastId, tailId, bodyId]);

  return {
    drakerImage,
    loading,
  };
}
