import { MaxUint256 } from "@ethersproject/constants";
import { useCallback } from "react";
import { calculateGasMargin } from "utils";
import { useTokenContract } from "./useContract";

// returns a variable indicating the state of the approval and a function which approves if necessary or early returns
export function useApproveCallbackCustom(
  token?: any,
  addressNeedApprove?: string
) {
  const tokenContract = useTokenContract(token);

  const approve = useCallback(async () => {
    const estimatedGas = await tokenContract.estimateGas
      .approve(addressNeedApprove, MaxUint256)
      .catch(() => {
        return tokenContract.estimateGas.approve(
          addressNeedApprove,
          MaxUint256
        );
      });

    try {
      const tx = await tokenContract.approve(addressNeedApprove, MaxUint256, {
        gasLimit: calculateGasMargin(estimatedGas),
      });

      await tx.wait();
    } catch (error) {
      console.debug("Failed to approve token", error);
      throw error;
    }
  }, [tokenContract, addressNeedApprove]);

  return [approve];
}
