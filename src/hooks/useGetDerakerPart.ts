import React, { useEffect, useState } from "react";
import useRefetch from "../zustand/useRefetch";
import useMarketplaceStore from "../zustand/marketplace/useMarketplaceStore";
const axios = require("axios");

export default function useGetDerakerPart(api: string, typePart?: string) {
  const [loading, setLoading] = useState(true);
  const [partList, setPartList] = useState([]);
  const { refetch, changeRefetchStatus } = useRefetch();

  useEffect(() => {
    const getNewsList = async () => {
      await axios({
        method: "get",
        url: `${api}`,
        params: { typePart },
      }).then(function (response) {
        console.log(
          "🚀 ~ file: useFetchData.ts ~ line 32 ~ getNewsList ~ response",
          response.data.data.results
        );
        setPartList(response.data.data.results);
        setLoading(false);
      });
    };

    getNewsList();
  }, [refetch]);

  return {
    partList,
    loading,
  };
}
