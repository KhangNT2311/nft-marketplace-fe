import { ethers } from "ethers";
import tokens from "config/constants/tokens";
import { getBusdAddress, getDrtAddress } from "utils/addressHelpers";

export const MARKETPLACE_STATUS = {
  INVENTORY: 0,
  ON_SALE: 1,
};

export const MARKET_ITEM_TYPE = {
  BOX: "BOX",
  SHOE: "SHOE",
};

export const paymentTokens = [
  {
    name: "BNB",
    address: {
      56: ethers.constants.AddressZero,
      97: ethers.constants.AddressZero,
    },
    image: "/images/tokens/bnb.png",
  },
  {
    name: "BUSD",
    address: tokens.busd.address,
    image: "/images/tokens/busd.png",
  },
  {
    name: "DRT",
    address: tokens.drt.address,
    image: "/images/tokens/drt.png",
  },
];

export const TAB = {
  GIFT: 0,
  LUCKY: 1,
};

export const NFT_TYPE = {
  BOX: 0,
  SHOE: 1,
};

export const BOX_TYPE = {
  GIFT: 0,
  LUCKY: 1,
};

export const SHOE_TYPE = {
  0: "MID1",
  1: "MID2",
  2: "BURN1",
  3: "BURN2",
  4: "VIPBURN",
};

export const boxTypes = ["LUCKY_BOX", "GIFT_BOX", "SHOE"];

export const boxes = [
  {
    name: "Gift box",
    img: "gift-box.png",
    type: BOX_TYPE.GIFT,
  },
  {
    name: "Lucky box",
    img: "box.png",
    type: BOX_TYPE.LUCKY,
  },
];

export const boxValues = {
  GIFT_BOX: {
    img: "gift-box.png",
  },
  LUCKY_BOX: {
    img: "box.png",
  },
};

export const getPaymentToken = (paymentToken: string) => {
  switch (paymentToken) {
    case ethers.constants.AddressZero:
      return {
        symbol: "BNB",
        img: "bnb.svg",
      };
    case getBusdAddress():
      return {
        symbol: "BUSD",
        img: "busd.svg",
      };
    case getDrtAddress():
      return {
        symbol: "STF",
        img: "stf.svg",
      };
    default:
      return null;
  }
};

export const FILTER_TYPE = {
  PAGE: "PAGE",
  SHOE_RARITY: "SHOE_RARITY",
  SHOE_TYPE: "SHOE_TYPE",
  PAYMENT_TOKEN: "PAYMENT_TOKEN",
  PRICE: "PRICE",
  LEVEL: "LEVEL",
  MINT_SLOT: "MINT_SLOT",
};
