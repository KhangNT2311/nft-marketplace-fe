import fire from '../assets/images/fire.svg';
import water from '../assets/images/water-icon.png';
import earth from '../assets/images/earth-icon.png';
import bodyShape from '../assets/images/parts/body-shape.png';
import beastShape from '../assets/images/parts/beast-shape.png';
import tailShape from '../assets/images/parts/tail-shape.png';
import beast1 from '../assets/images/beast/back.png'
import beast2 from '../assets/images/beast/beast-02.png'
import beast3 from '../assets/images/beast/back-2.png'
import tail1 from '../assets/images/tail/aquatic-03.png'
import tail2 from '../assets/images/tail/tail.png'
import tail3 from '../assets/images/tail/tail-2.png'
import body1 from '../assets/images/body/aquatic-03.png'
import body2 from '../assets/images/body/beast-04.png'
import body3 from '../assets/images/body/plant-02.png'
import fullBody1 from '../assets/images/fullbody/axie-full-transparent2.png'

const typeColor = {
  water: "#29b0ea",
  fire: "#ff1e1e",
  earth: "#575757",
};

const typeIcon = {
  fire: fire,
  water: water,
  earth: earth
}

const parts = {
  body: bodyShape,
  beast: beastShape,
  tail: tailShape
}

const beast = [
  beast2,
  beast1,
  beast3
]

const tail = [
  tail2,
  tail1,
  tail3
]

const body = [
  body1,
  body2,
  body3
]

const fullBody = [
  fullBody1
]

export default {
  typeColor,
  typeIcon,
  parts,
  beast,
  tail,
  body,
  fullBody
};
