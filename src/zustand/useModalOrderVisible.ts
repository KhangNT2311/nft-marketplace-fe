import create, { StateCreator } from "zustand";

interface modalOrderState {
  modalOrderStatus: boolean;
  changeModalOrderStatus: (navStatus: boolean) => void;
}

const useModalOrderVisible = create<modalOrderState>((set) => ({
  modalOrderStatus: false,
  changeModalOrderStatus: (modalOrderStatus) => set({ modalOrderStatus }),
}));

export default useModalOrderVisible;
