import create, { StateCreator } from "zustand";

interface TokenIdDrakerState {
    tokenId: string;
    changeTokenId: (tokenId: string) => void;
}

const useTokenIdDraker = create<TokenIdDrakerState>((set) => ({
    tokenId: "",
    changeTokenId: (tokenId) => set({ tokenId }),
}));

export default useTokenIdDraker;
