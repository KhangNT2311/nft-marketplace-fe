import create from "zustand";

const useMarketplaceStore = create((set, get) => ({
  listing: [],
  pagination: {
    marketPage: 1,
    limit: 1000,
    totalResults: 0,
  },
  fetching: {
    button: false,
  },
  isLoading: true,
  rebound: true,
  setMarketplaceState: async (data) => {
    set((state) => ({ ...state, ...data }));
  },
}));

export default useMarketplaceStore;
