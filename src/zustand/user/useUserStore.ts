import {
  getBusdAddress,
  getDrakerAddress,
  getDrtAddress,
  getMarketplaceAddress,
} from "utils/addressHelpers";
import multicall from "utils/multicall";
import create from "zustand";
import erc20ABI from "config/abi/erc20.json";
import { simpleRpcProvider } from "utils/providers";
import { getErc721Contract } from "utils/contractHelpers";

const useUserStore = create((set, get) => ({
  userTokenData: {
    drt: {
      balance: "0",
      allowance: "0",
      isAllowance: false,
    },
    busd: {
      balance: "0",
      allowance: "0",
      isAllowance: false,
    },
    bnb: {
      balance: "0",
    },
  },
  isApprovalForAll: false,
  isLoading: true,
  rebound: true,
  loaded: false,
  fetchUserTokenData: async (account: string) => {
    const calls = [];
    const drtToken = getDrtAddress();
    const busdToken = getBusdAddress();

    calls.push({ address: drtToken, name: "balanceOf", params: [account] });
    calls.push({ address: busdToken, name: "balanceOf", params: [account] });
    calls.push({
      address: drtToken,
      name: "allowance",
      params: [account, getMarketplaceAddress()],
    });
    calls.push({
      address: busdToken,
      name: "allowance",
      params: [account, getMarketplaceAddress()],
    });

    try {
      const [drtBalance, busdBalance, drtAllowance, busdAllowance] =
        await multicall(erc20ABI, calls, {
          requireSuccess: false,
        });

      const bnbBalance = await simpleRpcProvider.getBalance(account);
      set((state: any) => ({
        ...state,
        userTokenData: {
          drt: {
            balance: drtBalance.toString(),
            allowance: drtAllowance.toString(),
            isAllowance: +drtAllowance.toString() !== 0,
          },
          busd: {
            balance: busdBalance.toString(),
            allowance: busdAllowance.toString(),
            isAllowance: +busdAllowance.toString() !== 0,
          },
          bnb: {
            balance: bnbBalance.toString(),
          },
        },
        loaded: true,
      }));
    } catch (error) {
      set((state: any) => ({
        ...state,
        userTokenData: {
          drt: {
            balance: "0",
            allowance: "0",
          },
          busd: {
            balance: "0",
            allowance: "0",
          },
          bnb: {
            balance: "0",
          },
          loaded: true,
        },
      }));
    }
  },
  fetchApprovalNftMarket: async (account: string) => {
    try {
      const marketplaceAddress = getMarketplaceAddress();
      const characterContract = getErc721Contract(getDrakerAddress());
      const isApprovalForAll = await characterContract.isApprovedForAll(
        account,
        marketplaceAddress
      );

      set((state) => ({
        ...state,
        isApprovalForAll,
      }));
    } catch (e) {
      console.log("fetchApprovalNftMarket error", e);
    }
  },
  setUserState: async (data) => {
    set((state) => ({ ...state, ...data }));
  },
}));

export default useUserStore;
