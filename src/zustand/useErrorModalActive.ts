import create, { StateCreator } from "zustand";

interface errorModalState {
    errorModalStatus: boolean;
    changeErrorModalStatus: (navStatus: boolean) => void;
}

const useErrorModalActive = create<errorModalState>((set) => ({
    errorModalStatus: false,
    changeErrorModalStatus: (errorModalStatus) => set({ errorModalStatus }),
}));

export default useErrorModalActive;
