import create, { StateCreator } from "zustand";

interface filterBoxState {
    refetch: boolean;
    changeRefetchStatus: (navStatus: boolean) => void;
}

const useRefetch = create<filterBoxState>((set) => ({
    refetch: true,
    changeRefetchStatus: (refetch) => set({ refetch }),
}));

export default useRefetch;
